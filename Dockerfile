# name the portage image
FROM gentoo/portage:latest as portage

# based on stage3 image
FROM gentoo/stage3:amd64-openrc

# copy the entire portage volume in
COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

# Don't want to use things with different COMMON_FLAGS
RUN rm /etc/portage/binrepos.conf/*

ADD make.conf /etc/portage/
ADD ceph.use /etc/portage/package.use/

RUN mkdir /etc/portage/env /etc/portage/package.env
ADD j4.conf /etc/portage/env/
ADD ceph.env /etc/portage/package.env/

ADD world /var/lib/portage/

ADD iozone.license /etc/portage/package.license/
ADD linux-firmware.license /etc/portage/package.license/
ADD k8s.mask /etc/portage/package.mask/
ADD genkernel.use /etc/portage/package.use/
ADD grub.use /etc/portage/package.use/
ADD k8s.use /etc/portage/package.use/

RUN emerge app-editors/vim
